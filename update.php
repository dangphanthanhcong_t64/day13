<?php
session_start();

$genders = array(
    0 => "Nam",
    1 => "Nữ",
);
$faculties = array(
    "None" => "",
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu",
);

if (!isset($data)) {
    // Connect DB
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "formDB";
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, 'UTF8');

    // Get record from DB
    $query = "SELECT * FROM student";
    $result = mysqli_query($conn, $query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
}
if (!isset($_SESSION["keyword_name"])) {
    $_SESSION["keyword_name"] = "";
}
if (!isset($_SESSION["keyword_faculty"])) {
    $_SESSION["keyword_faculty"] = "";
}
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="style_update.css">
    <title>Update</title>
</head>

<body>
    <main>
        <form action="update.php" method="post">
            <div class="container">
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $_SESSION["keyword_name"] = $_POST["keyword_name"];
                    $_SESSION["keyword_faculty"] = $_POST["keyword_faculty"];

                    $filter = array();
                    $keyword_name = $_SESSION["keyword_name"];
                    $keyword_faculty = $_SESSION["keyword_faculty"];
                    if ($keyword_name != '') {
                        $filter[] = 'name LIKE ' . "'%$keyword_name%'";
                    }
                    if ($keyword_faculty != 'None') {
                        $filter[] = 'faculty = ' . "'$keyword_faculty'";
                    }
                    if (count($filter) == 0) {
                        $query = "SELECT * FROM student";
                    } else {
                        $query = "SELECT * FROM student WHERE " . implode(' AND ', $filter);
                    }
                    // echo $query;
                    $result = mysqli_query($conn, $query);
                    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
                }
                ?>
                <div>
                    <label for="keyword_faculty">Khoa</label>
                    <select name="keyword_faculty">
                        <?php
                        foreach ($faculties as $key => $value) {
                            if ($key == $_SESSION["keyword_faculty"]) {
                                $selected = "selected";
                            } else {
                                $selected = "";
                            }
                            echo "<option value=$key $selected>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div>
                    <label for="keyword_name">Từ khóa</label>
                    <input type="text" class="input" name="keyword_name" value='<?php echo ($_SESSION["keyword_name"]); ?>'>
                </div>

                <button type="submit" class="button" name="search">Tìm kiếm</button>

                <div>
                    Số sinh viên tìm thấy: <?php echo count($data); ?>
                </div>

                <div>
                    <button type="button" onclick="location.href = 'register.php';" class="button" name="register" style="margin: 8px 725px;">Thêm</button>
                </div>

                <?php
                if (count($data) > 0) {
                    echo "<table>
                        <tr>
                            <th>STT</th>
                            <th>Tên sinh viên</th>
                            <th>Khoa</th>
                            <th>Hành động</th>
                        </tr>";
                    $count = 1;
                    foreach ($data as $data) {
                        echo "<tr>
                        <td style='width: 100px;'>" . $count .
                            "<td style='width: 600px;'>" . $data["name"] .
                            "</td><td style='width: 400px;'>" . $faculties[$data["faculty"]] .
                            "</td><td style='width: 400px;'>" . "<button type='button' style='margin: 8px 8px; border-radius: 0px; text-align: center; width: auto;'>Sửa</button>" . "<button type='button' style='margin: 8px 8px; border-radius: 0px; text-align: center; width: auto;''>Xóa</button>" .
                            "</td>
                        </tr>";
                        $count++;
                    }
                    echo "</table>";
                }
                ?>

        </form>
    </main>
</body>

</html>